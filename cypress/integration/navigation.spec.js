/// <reference types="cypress" />

context('Navigation', () => {
    beforeEach(() => {
        cy.visit('http://localhost:8090/f1history/drivers')
    })

    it('Open drives standing page', () => {
        cy.get('a').contains('Drivers').click()
        cy.get('.ant-table-title').contains('div', /Season Drivers Standing/i)
    })

    it('Open constructors page', () => {
        cy.get('a').contains('Teams').click()
        cy.get('.ant-table-title').contains('div', /Season Constructors Standing/i)
    })

    it('Change season', () => {
        cy.get('a').contains('Drivers').click()
        cy.get('input').clear().type("2019")
        cy.get('table').contains('td', /Sergio Pérez/i)
    })

    it('Open and close driver results', () => {
        cy.get('a').contains('Drivers').click()
        cy.get('table').contains('td', /Lewis Hamilton/i).click()
        cy.get('table').contains('td', /Race/i)
        cy.get('table').contains('td', /Lewis Hamilton/i).click()
        !cy.get('table').contains('td', /Race/i)
    })
})
