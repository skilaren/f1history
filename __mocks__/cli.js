const ijlConfig = require('../ijl.config');

module.exports = {
    getConfig() {
        return ijlConfig.config
    },
    getNavigationsValue() {
        return ijlConfig.navigations
    }
}