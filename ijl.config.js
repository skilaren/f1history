const pkg = require("./package")

module.exports = {
    "apiPath": "stubs/api",
    "webpackConfig": {
        "output": {
            "publicPath": `/static/f1history/${pkg.version}/`
        },
        "module": {
            "rules": [
                { parser: { system: false } },
                {
                    "test": /\.tsx?$/,
                    "use": ['ts-loader'],
                    "exclude": /node_modules/
                },
                {
                    "test": /\.css$/,
                    "use": ['style-loader', 'css-loader']
                },
                {
                    "test": /\.(png|jp(e*)g|svg|gif)$/,
                    "use": ['file-loader']
                }
            ]
        },
        "resolve": {
            "extensions": ['.tsx', '.ts', '.js', 'jsx'],
        }
    },
    "navigations": {

    },
    config: {
        "f1history.api.base.url": "/api"
    }
}