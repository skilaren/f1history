import React, {PureComponent} from "react";

import {CountryImg} from './styled'


export class CountryFlag extends PureComponent<any, any> {
    render() {
        const {src, alt, type, ...rest} = this.props
        return (<CountryImg src={src} alt={alt}/>)
    }
}
