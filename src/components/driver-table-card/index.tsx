import React, {useState} from "react";
import {Table} from "antd";
import {connect} from "react-redux";
import getDriverSeasonResultsAction from "@main/__data__/actions/driverSeasonResults";
import {createStructuredSelector} from "reselect";
import {getError, getRaceTable} from "@main/__data__/selectors/driverSeasonResults";
import {Race} from "@main/__data__/models/interfaces";


const DriverTableCard = ({getDriverSeasonResults, raceResults, year, driverId, error}) => {
    const [driverResults, setDriverResults] = useState([])

    React.useEffect(() => {
        getDriverSeasonResults(year, driverId)
    }, [])

    React.useEffect(() => {
        if (raceResults != undefined) {
            if (driverId == raceResults[0].Results[0].Driver.driverId) {
                setDriverResults(raceResults)
            }
        }
    }, [raceResults])

    const getPositions = () => {
        let positions = {race: 'Position'}
        driverResults.forEach((element) => {
            positions[element.round] = element.Results[0].positionText
        })
        return [positions]
    }

    const getColumns = () => {
        let columns = [{title: 'Race', dataIndex: 'race', key: 'race'}]
        return columns.concat(driverResults.map(race => {
            return {title: race.raceName, dataIndex: race.round, key: race.round}
        }))
    }

    return (
        <div>
            {error == null ? (
                <Table
                    bordered
                    columns={getColumns()}
                    dataSource={getPositions()}
                    pagination={false}
                    rowKey={() => `${driverId}${year}`}
                    loading={driverResults.length == 0}
                />) : (
                <p>Error during data loading</p>
            )
            }
        </div>
    )
}

const mapStateToProps = (state, OwnProps) => createStructuredSelector({
    year: () => OwnProps.year,
    driverId: () => OwnProps.driverId,
    raceResults: getRaceTable,
    error: getError
})

export default connect(
    mapStateToProps,
    {
        getDriverSeasonResults: getDriverSeasonResultsAction,
    },
)(DriverTableCard);