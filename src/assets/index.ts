import gb from './svg/gb.svg'
import ru from './svg/ru.svg'
import fi from './svg/fi.svg'
import nl from './svg/nl.svg'
import th from './svg/th.svg'
import mc from './svg/mc.svg'
import fr from './svg/fr.svg'
import es from './svg/es.svg'
import mx from './svg/mx.svg'
import de from './svg/de.svg'
import it from './svg/it.svg'
import dk from './svg/dk.svg'
import ca from './svg/ca.svg'
import au from './svg/au.svg'
import ar from './svg/ar.svg'
import us from './svg/us.svg'
import ch from './svg/ch.svg'
import be from './svg/be.svg'
import at from './svg/at.svg'

const countries = {
    'British': gb,
    'Russian': ru,
    'Finnish': fi,
    'Dutch': nl,
    'Thai': th,
    'Monegasque': mc,
    'French': fr,
    'Spanish': es,
    'Mexican': mx,
    'German': de,
    'Italian': it,
    'Danish': dk,
    'Canadian': ca,
    'Australian': au,
    'American': us,
    'Argentine': ar,
    'Swiss': ch,
    'Belgian': be,
    'Austrian': at
}

export { countries }