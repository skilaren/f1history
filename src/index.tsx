import React from 'react'
import ReactDOM from 'react-dom'
import App from './App'
import 'antd/dist/antd.css'
import {applyMiddleware, createStore} from "redux";
import reducer from "@main/__data__/reducers";
import {composeWithDevTools} from "redux-devtools-extension";
import * as Thunk from "redux-thunk";
import {Provider} from "react-redux";
import {BrowserRouter as Router} from "react-router-dom"

export const mount = (Component: React.ComponentType) => {
    ReactDOM.render(
        <Provider store={createStore(reducer, composeWithDevTools(applyMiddleware(Thunk.default)))}>
            <Router>
                <App/>
            </Router>
        </Provider>,
        document.getElementById('app')
    )
}

export const unmount = () => {
    ReactDOM.unmountComponentAtNode(document.getElementById('app'))
}