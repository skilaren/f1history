import {InputNumber} from 'antd';
import {Table} from 'antd';

import React, {useState} from 'react';
import {connect} from "react-redux";

import {createStructuredSelector} from "reselect";

import {getDrivers, getError} from "@main/__data__/selectors/drivers";
import getDriverStandingAction from '@main/__data__/actions/driversStandings';
import {Constructor, Driver} from "@main/__data__/models/interfaces";

import {CountryFlag} from "@main/components/country-flag";

import {countries} from "@main/assets";
import DriverTableCard from "@main/components/driver-table-card";


const columns = [
    {
        title: 'Position',
        dataIndex: 'positionText',
        key: 'position'
    },
    {
        title: 'Country',
        dataIndex: ['Driver', 'nationality'],
        key: 'country',
        render: (nationality: string) => <CountryFlag src={countries[nationality]} alt={nationality}/>
    },
    {
        title: 'Driver',
        dataIndex: 'Driver',
        key: 'driver',
        render: (driver: Driver) => `${driver.givenName} ${driver.familyName}`
    },
    {
        title: 'Team',
        dataIndex: 'Constructors',
        key: 'team',
        render: (constructors: Array<Constructor>) => `${constructors[0].name}`
    },
    {
        title: 'Points',
        dataIndex: 'points',
        key: 'points'
    }
]

const DriversStandings = ({getDriverStanding, drivers, error}) => {
    const [year, setYear] = useState(0)

    React.useEffect(() => {
        getDriverStanding(2020)
        setYear(2020)
    }, [])

    const changeSeason = (data: number) => {
        if (1950 <= data && data <= 2020) {
            getDriverStanding(data)
            setYear(data)
        }
    };

    return (
        <Table
            columns={columns}
            dataSource={drivers}
            scroll={{x: true}}
            expandable={{
                expandRowByClick: true,
                expandedRowRender: (record) =>
                    <DriverTableCard style={{margin: 0}} year={year} driverId={record.Driver.driverId}/>,
                expandIconColumnIndex: -1,
            }}
            title={() => {
                return (
                    <div>
                        <InputNumber min={1950} max={2020} defaultValue={2020} onChange={changeSeason}
                                     style={{marginRight: 15}}/>
                        Season Drivers Standing
                    </div>
                )
            }}
            rowKey={(record) => `${year}${record.position}`}
            loading={error != null}
        />

    );
}

const mapStateToProps = () => createStructuredSelector({
    drivers: getDrivers,
    error: getError
})

export default connect(mapStateToProps, {
    getDriverStanding: getDriverStandingAction,
})(DriversStandings);
