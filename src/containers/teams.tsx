import {InputNumber} from 'antd';
import {Table} from 'antd';

import React from 'react';
import {connect} from "react-redux";

import {createStructuredSelector} from "reselect";

import {getConstructors, getError} from "@main/__data__/selectors/constructors";
import getConstructorStandingAction from '@main/__data__/actions/constructorsStandings'
import {Constructor} from "@main/__data__/models/interfaces";

import {CountryFlag} from "@main/components/country-flag";

import {countries} from "@main/assets";

const columns = [
    {
        title: 'Position',
        dataIndex: 'positionText',
    },
    {
        title: 'Country',
        dataIndex: ['Constructor', 'nationality'],
        render: (nationality: string) => <CountryFlag src={countries[nationality]} alt={nationality}/>
    },
    {
        title: 'Team',
        dataIndex: 'Constructor',
        render: (constructor: Constructor) => `${constructor.name}`
    },
    {
        title: 'Points',
        dataIndex: 'points',
    },
    {
        title: 'Wins',
        dataIndex: 'wins',
    }
]

const ConstructorStanding = ({getConstructorStanding, constructors, error}) => {
    React.useEffect(() => {
        getConstructorStanding(2020)
    }, [])

    const changeSeason = (data: number) => {
        if (1950 <= data && data <= 2020) {
            getConstructorStanding(data)
        }
    };

    return (
        <Table columns={columns} dataSource={constructors} title={() => {
            return (
                <div>
                    <InputNumber min={1950} max={2020} defaultValue={2020} onChange={changeSeason}
                                 style={{marginRight: 15}}/>
                    Season Constructors Standing
                </div>
            )
        }}/>
    );
}

const mapStateToProps = () => createStructuredSelector({
    constructors: getConstructors,
    error: getError
})

export default connect(mapStateToProps, {getConstructorStanding: getConstructorStandingAction})(ConstructorStanding);
