import React from "react";
import {describe, it, expect} from '@jest/globals'
import {mount, configure} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16'
import {applyMiddleware, createStore} from "redux";
import reducer from "@main/__data__/reducers";
import {composeWithDevTools} from "redux-devtools-extension";
import * as Thunk from "redux-thunk";
import {Provider} from "react-redux";
import {BrowserRouter as Router} from "react-router-dom"
import moxios from 'moxios'
import {jest} from '@jest/globals'
import {act} from 'react-dom/test-utils'
import '../../__mocks__/jsdom'
import ConstructorStanding from "@main/containers/teams";


configure({adapter: new Adapter()})

describe('Teams standing page', () => {
    beforeEach(() => {
        moxios.install();
        window.__webpack_public_path__ = '/';
    });

    afterEach(() => moxios.uninstall());

    it('renders', () => {
        const page = mount(
            <Provider store={createStore(reducer, composeWithDevTools(applyMiddleware(Thunk.default)))}>
                <Router>
                    <ConstructorStanding/>
                </Router>
            </Provider>
        )

        expect(page).toMatchSnapshot()
    })

    it('enter season year', async () => {
        const page = mount(
            <Provider store={createStore(reducer, composeWithDevTools(applyMiddleware(Thunk.default)))}>
                <Router>
                    <ConstructorStanding/>
                </Router>
            </Provider>
        )

        expect(page).toMatchSnapshot()

        await moxios.wait(jest.fn);

        page.find('input').simulate('change', {target: {name: 'year', value: '2019'}})

        await act(async () => {
            const request = moxios.requests.mostRecent();
            await request.respondWith({
                status: 200,
                response: require('../../stubs/api/constructorStandings/season2019')
            });
        });

        page.update();
        expect(page).toMatchSnapshot();
    })

    it('error from api', async () => {
        const page = mount(
            <Provider store={createStore(reducer, composeWithDevTools(applyMiddleware(Thunk.default)))}>
                <Router>
                    <ConstructorStanding/>
                </Router>
            </Provider>
        )

        await moxios.wait(jest.fn);

        await act(async () => {
            const request = moxios.requests.mostRecent();
            await request.respondWith({
                status: 500
            });
        });

        page.update()

    })
})