import React from "react";
import {describe, it, expect, jest} from '@jest/globals'
import {mount, configure} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16'
import {applyMiddleware, createStore} from "redux";
import reducer from "@main/__data__/reducers";
import {composeWithDevTools} from "redux-devtools-extension";
import * as Thunk from "redux-thunk";
import {Provider} from "react-redux";
import {BrowserRouter as Router} from "react-router-dom";
import moxios from 'moxios';
import '../../__mocks__/jsdom';
import DriversStandings from "@main/containers/drivers";
import {act} from "react-dom/test-utils";
import DriverTableCard from "@main/components/driver-table-card";
import App from "@main/App";


configure({adapter: new Adapter()})

describe('Driver standing page', () => {
    beforeEach(() => {
        moxios.install();
        window.__webpack_public_path__ = '/';
    });

    afterEach(() => moxios.uninstall());

    it('renders', () => {
        const page = mount(
            <Provider store={createStore(reducer, composeWithDevTools(applyMiddleware(Thunk.default)))}>
                <Router>
                    <DriversStandings/>
                </Router>
            </Provider>
        )

        expect(page).toMatchSnapshot()
    })

    it('enter season year', async () => {
        const page = mount(
            <Provider store={createStore(reducer, composeWithDevTools(applyMiddleware(Thunk.default)))}>
                <Router>
                    <DriversStandings/>
                </Router>
            </Provider>
        )

        expect(page).toMatchSnapshot()

        await moxios.wait(jest.fn);

        page.find('input').simulate('change', {target: {name: 'year', value: '2019'}})

        await act(async () => {
            const request = moxios.requests.mostRecent();
            await request.respondWith({
                status: 200,
                response: require('../../stubs/api/driversStandings/season2019')
            });
        });

        page.update();
        expect(page).toMatchSnapshot();
    })

    it('error from api', async () => {
        const page = mount(
            <Provider store={createStore(reducer, composeWithDevTools(applyMiddleware(Thunk.default)))}>
                <Router>
                    <DriversStandings/>
                </Router>
            </Provider>
        )

        await moxios.wait(jest.fn);

        await act(async () => {
            const request = moxios.requests.mostRecent();
            await request.respondWith({
                status: 500
            });
        });

        page.update()

    })

    it('driver results', async () => {
        const page = mount(
            <Provider store={createStore(reducer, composeWithDevTools(applyMiddleware(Thunk.default)))}>
                <DriverTableCard/>
            </Provider>
        )

        await moxios.wait(jest.fn);

        await act(async () => {
            const request = moxios.requests.mostRecent();
            await request.respondWith({
                status: 200,
                response: require('../../stubs/api/driversResults/hamilton2020.json')
            });
        });

        expect(page).toMatchSnapshot()


    })

    it('driver results error from api', async () => {
        const page = mount(
            <Provider store={createStore(reducer, composeWithDevTools(applyMiddleware(Thunk.default)))}>
                <DriverTableCard/>
            </Provider>
        )

        await moxios.wait(jest.fn);

        await act(async () => {
            const request = moxios.requests.mostRecent();
            await request.respondWith({
                status: 500,
            });
        });

        expect(page).toMatchSnapshot()

    })
})