import React from "react";
import {describe, it, expect} from '@jest/globals'
import {mount, configure} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16'
import {CountryFlag} from "../components/country-flag";

configure({adapter: new Adapter()})

describe ('Country flag component', () => {
    it('renders', () => {
        const page = mount(<CountryFlag src={"some/link/toImage.png"} alt={"country"}/>)
        expect(page).toMatchSnapshot()
    })
})