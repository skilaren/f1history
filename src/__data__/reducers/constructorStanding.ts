import {
    CONSTRUCTORS_STANDINGS_FETCH,
    CONSTRUCTORS_STANDINGS_FETCH_FAIL,
    CONSTRUCTORS_STANDINGS_FETCH_SUCCESS
} from '@main/__data__/constants/action-types';
import {MRData, AsyncDataState, MainDataRequest, ConstructorStandingList} from '@main/__data__/models/interfaces';
import {AnyAction} from 'redux';

type MainDataState = Partial<MainDataRequest<ConstructorStandingList>> & AsyncDataState<MRData<ConstructorStandingList>>;

const initialState: MainDataState = {
    driverSeasonResults: null,
    standingData: null,
    loading: false,
    error: null,
};

const fetchHandler = (state: MainDataState, action: AnyAction): MainDataState => ({
    ...state,
    loading: true,
});

const fetchSuccessHandler = (state: MainDataState, action: AnyAction): MainDataState => ({
    ...state,
    standingData: action.data!,
    loading: false,
});

const fetchErrorHandler = (state: MainDataState, action: AnyAction): MainDataState => ({
    ...state,
    standingData: null,
    loading: false,
    error: action.error
});

const handlers = {
    [CONSTRUCTORS_STANDINGS_FETCH]: fetchHandler,
    [CONSTRUCTORS_STANDINGS_FETCH_SUCCESS]: fetchSuccessHandler,
    [CONSTRUCTORS_STANDINGS_FETCH_FAIL]: fetchErrorHandler
};

export const constructorStanding = (state: MainDataState = initialState, action: AnyAction) =>
    Object.prototype.hasOwnProperty.call(handlers, action.type) ? handlers[action.type](state, action) : state;
