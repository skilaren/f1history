import {
    DRIVER_SEASON_RESULT_FETCH,
    DRIVER_SEASON_RESULT_FETCH_FAIL,
    DRIVER_SEASON_RESULT_FETCH_SUCCESS,
    DRIVERS_STANDINGS_FETCH,
    DRIVERS_STANDINGS_FETCH_FAIL,
    DRIVERS_STANDINGS_FETCH_SUCCESS
} from '@main/__data__/constants/action-types';
import {
    MRData,
    AsyncDataState,
    MainDataRequest,
    DriversStandingList,
    RaceTable
} from '@main/__data__/models/interfaces';
import {AnyAction} from 'redux';

type MainDataState = Partial<MainDataRequest<DriversStandingList>> & AsyncDataState<MRData<DriversStandingList>> & AsyncDataState<RaceTable>;

const initialState: MainDataState = {
    standingData: null,
    driverSeasonResults: null,
    loading: false,
    error: null,
};

const fetchHandler = (state: MainDataState, action: AnyAction): MainDataState => ({
    ...state,
    loading: true,
});

const fetchSuccessHandler = (state: MainDataState, action: AnyAction): MainDataState => ({
    ...state,
    standingData: action.data!,
    loading: false,
});

const fetchErrorHandler = (state: MainDataState, action: AnyAction): MainDataState => ({
    ...state,
    standingData: null,
    loading: false,
    error: action.error
});

const fetchDriverHandler = (state: MainDataState, action: AnyAction): MainDataState => ({
    ...state,
    loading: true,
});

const fetchDriverSuccessHandler = (state: MainDataState, action: AnyAction): MainDataState => ({
    ...state,
    driverSeasonResults: action.data!,
    loading: false,
});

const fetchDriverErrorHandler = (state: MainDataState, action: AnyAction): MainDataState => ({
    ...state,
    driverSeasonResults: null,
    loading: false,
    error: action.error
});

const handlers = {
    [DRIVERS_STANDINGS_FETCH]: fetchHandler,
    [DRIVERS_STANDINGS_FETCH_SUCCESS]: fetchSuccessHandler,
    [DRIVERS_STANDINGS_FETCH_FAIL]: fetchErrorHandler,
    [DRIVER_SEASON_RESULT_FETCH]: fetchDriverHandler,
    [DRIVER_SEASON_RESULT_FETCH_SUCCESS]: fetchDriverSuccessHandler,
    [DRIVER_SEASON_RESULT_FETCH_FAIL]: fetchDriverErrorHandler
};

export const driverStanding = (state: MainDataState = initialState, action: AnyAction) =>
    Object.prototype.hasOwnProperty.call(handlers, action.type) ? handlers[action.type](state, action) : state;
