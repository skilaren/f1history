import { combineReducers } from "redux";

import { driverStanding } from "./driverStanding";
import { constructorStanding } from "./constructorStanding";

export default combineReducers({
    driverStanding, constructorStanding
});