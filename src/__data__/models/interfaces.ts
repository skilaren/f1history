export interface AsyncDataState<T, E = string> {
    loading: boolean;
    standingData?: T;
    driverSeasonResults?: DriverSeasonResults,
    error?: E;
}

export interface MainDataRequest<T> {
    MRData: MRData<T>;
}

export interface DriverSeasonResults {
    MRData: MRData<any>;
}

export interface MRData<T> {
    StandingsTable?: StandingsTable<T>,
    RaceTable?: RaceTable,
    limit: string,
    offset: string,
    series: string,
    total: string,
    url: string
}

export interface RaceTable {
    Races: Array<Race>,
    season: string,
    driverId: string
}

export interface Race {
    season: string,
    round: string,
    url: string,
    raceName: string,
    Circuit: Circuit,
    date: string,
    time: string,
    Results: Array<Result>,
}

export interface Circuit {
    circuitId: string,
    url: string,
    circuitName: string,
    Location: Location
}

export interface Location {
    lat: string,
    long: string,
    locality: string,
    country: string
}

export interface Result {
    number: string,
    position: string,
    positionText: string,
    points: string
    Driver: Driver,
    Constructor: Constructor,
    grid: string,
    laps: string,
    status: string,
    Time: Time,
    FastestLap: FastestLap,
}

export interface Time {
    millis: string,
    time: string
}

export interface FastestLap {
    rank: string,
    lap: string,
    Time: Time,
    AverageSpeed: AverageSpeed
}

export interface AverageSpeed {
    units: string,
    speed: string
}

export interface StandingsTable<T> {
    season: string,
    StandingsLists: Array<T>
}

export interface DriversStandingList {
    round: string,
    season: string,
    DriverStandings: Array<DriverStanding>
}

export interface ConstructorStandingList {
    round: string,
    season: string,
    ConstructorStandings: Array<ConstructorStanding>
}

export interface DriverStanding {
    Constructors: Array<Constructor>
    "position": string,
    "positionText": string,
    "points": string,
    "wins": string,
    "Driver": Driver
}

export interface ConstructorStanding {
    position: string,
    positionText: string,
    points: string,
    wins: string,
    Constructor: Constructor
}

export interface Driver {
    "driverId": string,
    "permanentNumber": string,
    "code": string,
    "url": string,
    "givenName": string,
    "familyName": string,
    "dateOfBirth": string,
    "nationality": string
}

export interface Constructor {
    constructorId: string,
    name: string,
    nationality: string,
    url: string
}