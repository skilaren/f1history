import axios, {AxiosRequestConfig, AxiosResponse} from 'axios'
import {
    CONSTRUCTORS_STANDINGS_FETCH,
    CONSTRUCTORS_STANDINGS_FETCH_FAIL,
    CONSTRUCTORS_STANDINGS_FETCH_SUCCESS
} from "@main/__data__/constants/action-types";
import {ConstructorStandingList, MainDataRequest} from "@main/__data__/models/interfaces";
import {getConfig} from "@ijl/cli"

const getFetchAction = () => ({
    type: CONSTRUCTORS_STANDINGS_FETCH,
})

const getSuccessAction = (data) => ({
    type: CONSTRUCTORS_STANDINGS_FETCH_SUCCESS,
    data
})

const getErrorAction = () => ({
    type: CONSTRUCTORS_STANDINGS_FETCH_FAIL,
})

export default (year: number) => async (dispatch: any) => {
    dispatch(getFetchAction())

    const requestProps: AxiosRequestConfig = {
        method: 'get',
        headers: {
            'Content-Type': 'application/json'
        }
    }

    const f1historyApiBaseUrl = getConfig()['f1history.api.base.url']
    try {
        const answer: AxiosResponse<MainDataRequest<ConstructorStandingList>> = await axios(
            `${f1historyApiBaseUrl}/f1/${year}/constructorStandings.json`,
            requestProps
        )
        console.log('CONSTRUCTORS STANDING ACTION')
        console.log("SUCCESS")
        dispatch(getSuccessAction(answer))
    } catch (error) {
        dispatch(getErrorAction())
    }
}