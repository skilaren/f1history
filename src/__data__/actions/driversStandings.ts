import axios, {AxiosRequestConfig, AxiosResponse} from 'axios'
import {
    DRIVERS_STANDINGS_FETCH,
    DRIVERS_STANDINGS_FETCH_FAIL,
    DRIVERS_STANDINGS_FETCH_SUCCESS
} from "@main/__data__/constants/action-types";
import {DriversStandingList, MainDataRequest} from "@main/__data__/models/interfaces";
import {getConfig} from "@ijl/cli"

const getFetchAction = () => ({
    type: DRIVERS_STANDINGS_FETCH,
})

const getSuccessAction = (data) => ({
    type: DRIVERS_STANDINGS_FETCH_SUCCESS,
    data
})

const getErrorAction = () => ({
    type: DRIVERS_STANDINGS_FETCH_FAIL,
})

export default (year: number) => async (dispatch: any) => {
    dispatch(getFetchAction())

    const requestProps: AxiosRequestConfig = {
        method: 'get',
        headers: {
            'Content-Type': 'application/json'
        }
    }

    const f1historyApiBaseUrl = getConfig()['f1history.api.base.url']
    try {
        const answer: AxiosResponse<MainDataRequest<DriversStandingList>> = await axios(
            `${f1historyApiBaseUrl}/f1/${year}/driverStandings.json`,
            requestProps
        )
        console.log('DRIVERS STANDING ACTION')
        console.log("SUCCESS")
        dispatch(getSuccessAction(answer))
    } catch (error) {
        dispatch(getErrorAction())
    }
}