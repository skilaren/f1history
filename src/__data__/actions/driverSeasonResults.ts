import axios, {AxiosRequestConfig, AxiosResponse} from 'axios'
import {
    DRIVER_SEASON_RESULT_FETCH,
    DRIVER_SEASON_RESULT_FETCH_FAIL,
    DRIVER_SEASON_RESULT_FETCH_SUCCESS
} from "@main/__data__/constants/action-types";
import {DriverSeasonResults} from "@main/__data__/models/interfaces";
import {getConfig} from "@ijl/cli"

const getFetchAction = () => ({
    type: DRIVER_SEASON_RESULT_FETCH,
})

const getSuccessAction = (data) => ({
    type: DRIVER_SEASON_RESULT_FETCH_SUCCESS,
    data
})

const getErrorAction = () => ({
    type: DRIVER_SEASON_RESULT_FETCH_FAIL,
})

export default (year: number, driver: string) => async (dispatch: any) => {
    dispatch(getFetchAction())

    const requestProps: AxiosRequestConfig = {
        method: 'get',
        headers: {
            'Content-Type': 'application/json'
        }
    }

    const f1historyApiBaseUrl = getConfig()['f1history.api.base.url']
    try {
        console.log('DRIVER RESULTS ACTION')
        const answer: AxiosResponse<DriverSeasonResults> = await axios(
            `${f1historyApiBaseUrl}/f1/${year}/drivers/${driver}/results.json`,
            requestProps
        )
        console.log("SUCCESS")
        dispatch(getSuccessAction(answer))
    } catch (error) {
        dispatch(getErrorAction())
    }
}