import { createSelector } from 'reselect';

const getStore = state => state.driverStanding;

const getMainData = createSelector(getStore, (state) => state.standingData);

export const getDrivers = createSelector(getMainData, (state) => state?.data.MRData.StandingsTable.StandingsLists[0].DriverStandings)

export const getError = createSelector(getStore, (state) => state.error);
