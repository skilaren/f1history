import { createSelector } from 'reselect';

const getStore = state => state.driverStanding;

const getMainData = createSelector(getStore, (state) => state.driverSeasonResults);

export const getRaceTable = createSelector(getMainData, (state) => state?.data.MRData.RaceTable.Races)

export const getError = createSelector(getStore, (state) => state.error);
