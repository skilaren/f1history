import { createSelector } from 'reselect';

const getStore = state => state.constructorStanding;

const getMainData = createSelector(getStore, (state) => state.standingData);

export const getConstructors = createSelector(getMainData, (state) => state?.data.MRData.StandingsTable.StandingsLists[0].ConstructorStandings)

export const getError = createSelector(getStore, (state) => state.error);