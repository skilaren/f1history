import {Layout, Menu} from 'antd';

import React from 'react';

import {Switch, Route, Link} from 'react-router-dom';

import TeamsStandings from "@main/containers/teams"
import DriversStandings from "@main/containers/drivers";

const {Header, Content, Footer} = Layout;


const App = () => {
    return (
        <Layout className="layout">
            <Header>
                <Menu theme="dark" mode="horizontal" defaultSelectedKeys={["1"]}>
                    <Menu.Item key="1">
                        <Link to="/f1history/">Seasons</Link>
                    </Menu.Item>
                    <Menu.Item key="2">
                        <Link to="/f1history/drivers">Drivers</Link>
                    </Menu.Item>
                    <Menu.Item key="3">
                        <Link to="/f1history/teams">Teams</Link>
                    </Menu.Item>
                </Menu>
            </Header>
            <Content style={{padding: '0 50px'}}>
                <Switch>
                    <Route exact path="/f1history/" component={() => {
                        return <h1>Main</h1>
                    }}/>
                    <Route exact path="/f1history/drivers" component={DriversStandings}/>
                    <Route exact path="/f1history/teams" component={TeamsStandings}/>
                </Switch>
            </Content>
            <Footer style={{textAlign: 'center'}}>Maxim Burov for EPJA course.</Footer>
        </Layout>
    );
}

export default App