import {describe, it, expect} from '@jest/globals'

const sum = (a: number, b: number): number => a + b

describe ('trying tests', () => {
    it('adding 1 + 1 = 2', () => {
        expect(sum(1, 1)).toBe(2)
    })
})