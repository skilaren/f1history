const fs = require("fs");
const path = require("path");
const flow = require("./flow.json")

const router = require('express').Router()

router.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

const loadJson = (filepath, encoding = "utf8") =>
    JSON.parse(
        fs.readFileSync(path.resolve(__dirname, `${filepath}.json`), {encoding})
    );

router.get('/f1/:year/driverStandings.json', (req, res) => {
    res.send(
        loadJson(`driversStandings/season${req.params.year}`)
    )
})

router.get('/f1/:year/constructorStandings.json', (req, res) => {
    res.send(
        loadJson(`constructorStandings/season${req.params.year}`)
    )
})


router.get('/f1/:year/drivers/:driverId/results.json', (req, res) => {
    res.send(
        loadJson(`driversResults/${req.params.driverId}${req.params.year}`)
    )
})

router.get('/workFlow', (req, res) => {
    const {cmd, name} = req.query
    let stateName, flowName, process
    if (cmd === 'start') {
        flowName = name
        stateName = flow.flows[flowName].init
    }
    if (cmd === 'event') {
        flowName = req.session.workFlow.flowName
        const currentState = req.session.workFlow.stateName
        stateName = flow.flows[flowName].states[currentState].events[name].newState
    }
    if (cmd === 'exit') {
        req.session.workFlow = {}
        process = 'end'
    }
    req.session.workFlow = {
        flowName, stateName
    }
    res.send({
        flowName, stateName, process
    })
})

module.exports = router
